<?php declare(strict_types=1);

namespace Crehler\Faq\Migration;
use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1615281236faq extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1615281236;
    }

    public function update(Connection $connection): void
    {
        $connection->executeUpdate('
            CREATE TABLE IF NOT EXISTS `crehler_faq_category` (
              `id` BINARY(16) NOT NULL,
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');

        $connection->executeUpdate('
            CREATE TABLE IF NOT EXISTS `crehler_faq_category_translation` (
              `crehler_faq_category_id` BINARY(16) NOT NULL,
              `language_id` BINARY(16) NOT NULL,
              `name` VARCHAR(255),
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL,
              PRIMARY KEY (`crehler_faq_category_id`, `language_id`),
              CONSTRAINT `fk.crehler_faq_category_translation.crehler_faq_category_id` FOREIGN KEY (`crehler_faq_category_id`)
                REFERENCES `crehler_faq_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `fk.crehler_faq_category_translation.language_id` FOREIGN KEY (`language_id`)
                REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');

        $connection->executeUpdate('
            CREATE TABLE IF NOT EXISTS `crehler_faq` (
              `id` BINARY(16) NOT NULL,
              `crehler_faq_category_id` BINARY(16) NOT NULL,
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL,
               PRIMARY KEY (`id`,`crehler_faq_category_id`),
               CONSTRAINT `fk.crehler_faq.crehler_faq_category_id` FOREIGN KEY (`crehler_faq_category_id`)
                REFERENCES `crehler_faq_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');

        $connection->executeUpdate('
            CREATE TABLE IF NOT EXISTS `crehler_faq_translation` (
              `crehler_faq_id` BINARY(16) NOT NULL,
              `language_id` BINARY(16) NOT NULL,
              `question` VARCHAR(255),
              `answer` longtext,
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL,
              PRIMARY KEY (`crehler_faq_id`, `language_id`),
              CONSTRAINT `fk.crehler_faq_translation.crehler_faq_id` FOREIGN KEY (`crehler_faq_id`)
                REFERENCES `crehler_faq` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `fk.crehler_faq_translation.language_id` FOREIGN KEY (`language_id`)
                REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
    }

    public function updateDestructive(Connection $connection): void
    {

    }
}
