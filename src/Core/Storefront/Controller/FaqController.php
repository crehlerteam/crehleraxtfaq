<?php declare(strict_types=1);

namespace Crehler\Faq\Core\Storefront\Controller;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Crehler\Faq\Faq\FaqPageLoader;
/**
 * @RouteScope(scopes={"storefront"})
 */
class FaqController extends StorefrontController
{
    /**
    * @var FaqPageLoader
    */
    protected $faqPageLoader;

    public function __construct(
        FaqPageLoader $faqPageLoader
    )
    {
        $this->faqPageLoader = $faqPageLoader;
    }

    /**
     * @Route("/faq", name="frontend.crehler.faq", options={"seo"="false"}, methods={"GET"})
     */
    public function getFaq(SalesChannelContext $context,Request $request): Response
    {
        $page = $this->faqPageLoader->load($request, $context);

        return $this->renderStorefront('@CrehlerFaq/storefront/page/content/faq/faq.html.twig', ['page' => $page]);
    }
}
