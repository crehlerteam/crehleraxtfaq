<?php declare(strict_types=1);

namespace Crehler\Faq\Core\Content\FaqCategory;

use Crehler\Faq\Core\Content\Faq\FaqDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslatedField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslationsAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Crehler\Faq\Core\Content\FaqCategory\Aggregate\FaqCategoryTranslation\FaqCategoryTranslationDefinition;

class FaqCategoryDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'crehler_faq_category';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }
    public function getEntityClass(): string
    {
        return FaqCategoryEntity::class;
    }
    public function getCollectionClass(): string
    {
        return FaqCategoryCollection::class;
    }
    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
            new TranslatedField('name'),
            new TranslationsAssociationField(FaqCategoryTranslationDefinition::class, 'crehler_faq_category_id'),
            (new OneToManyAssociationField('faqs', FaqDefinition::class, 'crehler_faq_category_id')),
        ]);
    }
}
