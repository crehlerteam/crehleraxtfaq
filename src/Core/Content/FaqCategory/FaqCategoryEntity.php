<?php declare(strict_types=1);

namespace Crehler\Faq\Core\Content\FaqCategory;
use Crehler\Faq\Core\Content\Faq\FaqCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class FaqCategoryEntity extends Entity
{
    use EntityIdTrait;
    /**
     * @var string
     */
    protected $name;

    /**
     * @var FaqCollection|null
     */
    protected $faqs;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return FaqCollection|null
     */
    public function getFaqs(): ?FaqCollection
    {
        return $this->faqs;
    }

    /**
     * @param FaqCollection|null $faqs
     * @return FaqCategoryEntity
     */
    public function setFaqs(?FaqCollection $faqs): FaqCategoryEntity
    {
        $this->faqs = $faqs;
        return $this;
    }

}
