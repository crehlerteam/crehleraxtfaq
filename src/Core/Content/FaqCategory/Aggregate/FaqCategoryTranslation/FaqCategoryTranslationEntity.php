<?php declare(strict_types=1);

namespace Crehler\Faq\Core\Content\FaqCategory\Aggregate\FaqCategoryTranslation;
use Shopware\Core\Framework\DataAbstractionLayer\TranslationEntity;
use Crehler\Faq\Core\Content\FaqCategory\FaqCategoryEntity;

class FaqCategoryTranslationEntity extends TranslationEntity
{
    /**
     * @var string|null
     */
    protected $name;
    /**
     * @var FaqCategoryEntity
     */
    protected $faqCategory;

    public function getName(): ?string
    {
        return $this->name;
    }
    public function setName(string $name): void
    {
        $this->name = $name;
    }
    public function getFaqCategory(): FaqCategoryEntity
    {
        return $this->faqCategory;
    }
    public function setFaqCategory(FaqCategoryEntity $faqCategory): void
    {
        $this->faqCategory = $faqCategory;
    }
}