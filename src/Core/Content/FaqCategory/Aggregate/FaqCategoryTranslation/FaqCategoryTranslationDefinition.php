<?php declare(strict_types=1);

namespace Crehler\Faq\Core\Content\FaqCategory\Aggregate\FaqCategoryTranslation;
use Shopware\Core\Framework\DataAbstractionLayer\EntityTranslationDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Crehler\Faq\Core\Content\FaqCategory\FaqCategoryDefinition;

class FaqCategoryTranslationDefinition extends EntityTranslationDefinition
{
    public const ENTITY_NAME = 'crehler_faq_category_translation';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }
    public function getCollectionClass(): string
    {
        return FaqCategoryTranslationCollection::class;
    }
    public function getEntityClass(): string
    {
        return FaqCategoryTranslationEntity::class;
    }
    public function getParentDefinitionClass(): string
    {
        return FaqCategoryDefinition::class;
    }
    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new StringField('name', 'name'))->addFlags(new Required()),
        ]);
    }
}
