<?php declare(strict_types=1);

namespace Crehler\Faq\Core\Content\Faq\Aggregate\FaqTranslation;
use Shopware\Core\Framework\DataAbstractionLayer\TranslationEntity;
use Crehler\Faq\Core\Content\Faq\FaqEntity;

class FaqTranslationEntity extends TranslationEntity
{
    /**
     * @var string|null
     */
    protected $question;
    /**
     * @var string|null
     */
    protected $answer;
    /**
     * @var FaqEntity
     */
    protected $faq;

    public function getQuestion(): ?string
    {
        return $this->question;
    }
    public function setQuestion(string $question): void
    {
        $this->question = $question;
    }
    public function getAnswer(): ?string
    {
        return $this->answer;
    }
    public function setAnswer(string $answer): void
    {
        $this->answer = $answer;
    }
    public function getFaq(): FaqEntity
    {
        return $this->faq;
    }
    public function setFaq(FaqEntity $faq): void
    {
        $this->faq = $faq;
    }
}