<?php declare(strict_types=1);

namespace Crehler\Faq\Core\Content\Faq;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class FaqEntity extends Entity
{
    use EntityIdTrait;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $question;
    /**
     * @var string
     */
    protected $answer;
      /**
     * @var FaqCategoryCollection|null
     */
    protected $faqCategory;

    public function getName(): ?string
    {
        return $this->name;
    }
    public function setName(string $name): void
    {
        $this->name = $name;
    }
    public function getQuestion(): ?string
    {
        return $this->question;
    }
    public function setQuestion(string $question): void
    {
        $this->question = $question;
    }
    public function getAnswer(): ?string
    {
        return $this->answer;
    }
    public function setAnswer(string $answer): void
    {
        $this->answer = $answer;
    }
    public function getFaqCategory(): ?FaqCategoryCollection
    {
        return $this->faqCategory;
    }
    public function setFaqCategory(FaqCategoryCollection $faqCategory): void
    {
        $this->faqCategory = $faqCategory;
    }
}