<?php declare(strict_types=1);

namespace Crehler\Faq\Core\Content\Faq;

use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslatedField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslationsAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Crehler\Faq\Core\Content\Faq\Aggregate\FaqTranslation\FaqTranslationDefinition;
use Crehler\Faq\Core\Content\FaqCategory\FaqCategoryDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;

class FaqDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'crehler_faq';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }
    public function getEntityClass(): string
    {
        return FaqEntity::class;
    }
    public function getCollectionClass(): string
    {
        return FaqCollection::class;
    }
    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
            new TranslatedField('question'),
            new TranslatedField('answer'),
            (new FkField('crehler_faq_category_id','crehlerFaqCategoryId',FaqCategoryDefinition::class))->addFlags(new Required(), new CascadeDelete()),
            new TranslationsAssociationField(FaqTranslationDefinition::class, 'crehler_faq_id'),
            new ManyToOneAssociationField('crehlerFaqCategory', 'crehler_faq_category_id',FaqCategoryDefinition::class, 'id')
        ]);
    }
}
