<?php declare(strict_types=1);

namespace Crehler\Faq\Faq;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpFoundation\Request;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepositoryInterface;
use Shopware\Storefront\Page\GenericPageLoaderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;

class FaqPageLoader
{
    /**
     * @var GenericPageLoaderInterface
     */
    private $genericLoader;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
    * @var EntityRepositoryInterface
    */
    private $faqRepository;

    /**
    * @var EntityRepositoryInterface
    */
    private $faqCategoryRepository;

    public function __construct(
        GenericPageLoaderInterface $genericLoader,
        EventDispatcherInterface $eventDispatcher,
        EntityRepositoryInterface $faqCategoryRepository
    ) {
        $this->genericLoader = $genericLoader;
        $this->eventDispatcher = $eventDispatcher;
        $this->faqCategoryRepository = $faqCategoryRepository;
      }

    public function load(Request $request, SalesChannelContext $salesChannelContext): FaqPage
    {
        $page = $this->genericLoader->load($request, $salesChannelContext);
        $page = FaqPage::createFrom($page);
        $criteria = new Criteria();
        $criteria->addSorting(new FieldSorting('createdAt', 'ASC'));
        $criteria->addSorting(new FieldSorting('faqs.createdAt', 'ASC'));
        $criteria->addAssociation('faqs');
        $data = $this->faqCategoryRepository->search($criteria, $salesChannelContext->getContext());
        $page->setFaq($data->getElements());
        $this->eventDispatcher->dispatch(
            new FaqPageLoadedEvent($page, $salesChannelContext, $request)
        );
        return $page;
    }
}
