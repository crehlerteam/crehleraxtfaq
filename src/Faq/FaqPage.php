<?php declare(strict_types=1);

namespace Crehler\Faq\Faq;
use Shopware\Storefront\Page\Page;
use Crehler\Faq\Core\Content\Faq\FaqEntity;

class FaqPage extends Page
{
    /** @var array|null */
    protected $faq;

    /**
     * @return array|null
     */
    public function getFaq(): ?array
    {
        return $this->faq;
    }

    /**
     * @param array|null $faq
     * @return FaqPage
     */
    public function setFaq(?array $faq): FaqPage
    {
        $this->faq = $faq;
        return $this;
    }


}
