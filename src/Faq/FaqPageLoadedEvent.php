<?php declare(strict_types=1);

namespace Crehler\Faq\Faq;

use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpFoundation\Request;
use Shopware\Storefront\Page\PageLoadedEvent;
use Crehler\Faq\Faq\FaqPage;

class FaqPageLoadedEvent extends PageLoadedEvent
{
    /**
     * @var FaqPage
     */
    protected $page;

    public function __construct(FaqPage $page, SalesChannelContext $salesChannelContext, Request $request)
    {
        $this->page = $page;
        parent::__construct($salesChannelContext, $request);
    }

    public function getPage(): FaqPage
    {
        return $this->page;
    }

}
