import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'crehler-faq',
    category: 'sidebar',
    label: 'FAQ section',
    component: 'cr-cms-block-crehler-faq',
    previewComponent: 'cr-cms-preview-crehler-faq',
    defaultConfig: {
        marginBottom: '0',
        marginTop: '0',
        marginLeft: '0',
        marginRight: '0',
        sizingMode: 'boxed'
    },
    slots: {
        content: 'category-navigation'
    }
});
