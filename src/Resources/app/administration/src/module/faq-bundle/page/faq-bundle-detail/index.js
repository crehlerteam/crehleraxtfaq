import template from './faq-bundle-detail.html.twig';

const { Component, Mixin } = Shopware;

Component.register('faq-bundle-detail', {
    template,

    inject: [
        'repositoryFactory'
    ],

    mixins: [
        Mixin.getByName('notification')
    ],

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    data() {
        return {
            faq: null,
            isLoading: false,
            processSuccess: false,
            repository: null,
            isNew: false
        };
    },

    created() {
        this.repository = this.repositoryFactory.create('crehler_faq');
        this.getFaq();
        this.createdComponent();
    },

    methods: {

        createdComponent() {
            if (!this.$route.params.id) {
                // set language to system language
                if (!Shopware.State.getters['context/isSystemDefaultLanguage']) {
                    Shopware.State.commit('context/resetLanguageToDefault');
                }
            }
        },

        getFaq() {
            this.repository
                .get(this.$route.params.id, Shopware.Context.api)
                .then((entity) => {
                    this.faq = entity;
                });
        },

        onClickSave() {
            this.isLoading = true;

            this.repository
                .save(this.faq, Shopware.Context.api)
                .then(() => {
                    this.getFaq();
                    this.isLoading = false;
                    this.processSuccess = true;
                }).catch((exception) => {
                    this.isLoading = false;
                    this.createNotificationError({
                        title: this.$t('faq-bundle.detail.errorTitle'),
                        message: exception
                    });
                });
        },

        saveFinish() {
            this.processSuccess = false;
        },

        saveOnLanguageChange() {
            return this.onSave();
        },

        onChangeLanguage(languageId) {
            Shopware.State.commit('context/setApiLanguageId', languageId);
            this.getFaq();
        },
    }
});
