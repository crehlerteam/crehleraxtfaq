const { Component } = Shopware;

Component.extend('faq-bundle-create', 'faq-bundle-detail', {
    data() {
        return {
            isNew: true
        }
    },

    methods: {
        getFaq() {
            this.faq = this.repository.create(Shopware.Context.api);
        },

        onClickSave() {
            this.isLoading = true;

            this.repository
                .save(this.faq, Shopware.Context.api)
                .then(() => {
                    this.isLoading = false;
                    this.$router.push({ name: 'faq.bundle.detail', params: { id: this.faq.id } });
                }).catch((exception) => {
                    this.isLoading = false;

                    this.createNotificationError({
                        title: this.$t('faq-bundle.detail.errorTitle'),
                        message: exception
                    });
                });
        }
    }
});