import template from './faq-bundle-list.html.twig';

const { Component } = Shopware;
const { Criteria } = Shopware.Data;

Component.register('faq-bundle-list', {
    template,

    inject: [
        'repositoryFactory'
    ],

    data() {
        return {
            repository: null,
            faq: null
        };
    },

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    computed: {
        columns() {
            return [{
                property: 'question',
                dataIndex: 'question',
                label: this.$t('faq-bundle.list.columnName'),
                routerLink: 'faq.bundle.detail',
                inlineEdit: 'string',
                allowResize: true,
                primary: true
            }];
        }
    },

    created() {
        this.repository = this.repositoryFactory.create('crehler_faq');
        this.getFaqList();
    },

    methods: {
        getFaqList() {
            this.repository
                .search(new Criteria(), Shopware.Context.api)
                .then((result) => {
                    this.faq = result;
                });
        },

        abortOnLanguageChange() {
            return Shopware.State.getters['swProductDetail/hasChanges'];
        },

        saveOnLanguageChange() {
            return this.onSave();
        },

        onChangeLanguage(languageId) {
            Shopware.State.commit('context/setApiLanguageId', languageId);
            this.getFaqList();
        },
    }
});
