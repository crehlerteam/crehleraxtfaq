import './page/faq-bundle-list';
import './page/faq-bundle-detail';
import './page/faq-bundle-create';
import deDE from './snippet/de-DE.json';
import enGB from './snippet/en-GB.json';

const { Module } = Shopware;

Module.register('faq-bundle', {
    type: 'plugin',
    name: 'faq-bundle.general.mainMenuItemGeneral',
    title: 'faq-bundle.general.mainMenuItemGeneral',
    description: 'faq-bundle.general.descriptionTextModule',
    color: '#ff3d58',
    icon: 'default-action-bulk-edit',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        list: {
            component: 'faq-bundle-list',
            path: 'list'
        },
        detail: {
            component: 'faq-bundle-detail',
            path: 'detail/:id',
            meta: {
                parentPath: 'faq.bundle.list'
            }
        },
        create: {
            component: 'faq-bundle-create',
            path: 'create',
            meta: {
                parentPath: 'faq.bundle.list'
            }
        }
    },

    navigation: [{
        label: 'faq-bundle.general.mainMenuItemGeneral',
        color: '#ff3d58',
        path: 'faq.bundle.list',
        icon: 'default-action-bulk-edit',
        parent: 'sw-content'
    }]
});