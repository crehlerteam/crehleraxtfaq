import './page/faq-category-bundle-list';
import './page/faq-category-bundle-detail';
import './page/faq-category-bundle-create';
import deDE from './snippet/de-DE.json';
import enGB from './snippet/en-GB.json';

const { Module } = Shopware;

Module.register('faq-category-bundle', {
    type: 'plugin',
    name: 'faq-category-bundle.general.mainMenuItemGeneral',
    title: 'faq-category-bundle.general.mainMenuItemGeneral',
    description: 'faq-category-bundle.general.descriptionTextModule',
    color: '#ff3d58',
    icon: 'default-action-quickjump',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        list: {
            component: 'faq-category-bundle-list',
            path: 'list'
        },
        detail: {
            component: 'faq-category-bundle-detail',
            path: 'detail/:id',
            meta: {
                parentPath: 'faq.category.bundle.list'
            }
        },
        create: {
            component: 'faq-category-bundle-create',
            path: 'create',
            meta: {
                parentPath: 'faq.category.bundle.list'
            }
        }
    },

    navigation: [{
        label: 'faq-category-bundle.general.mainMenuItemGeneral',
        color: '#ff3d58',
        path: 'faq.category.bundle.list',
        icon: 'default-action-quickjump',
        parent: 'sw-content'
    }]
});