import template from './faq-category-bundle-list.html.twig';

const { Component } = Shopware;
const { Criteria } = Shopware.Data;

Component.register('faq-category-bundle-list', {
    template,

    inject: [
        'repositoryFactory'
    ],

    data() {
        return {
            repository: null,
            faqCategory: null
        };
    },

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    computed: {
        columns() {
            return [{
                property: 'name',
                dataIndex: 'name',
                label: this.$t('faq-category-bundle.list.columnName'),
                routerLink: 'faq.category.bundle.detail',
                inlineEdit: 'string',
                allowResize: true,
                primary: true
            }];
        }
    },

    created() {
        this.repository = this.repositoryFactory.create('crehler_faq_category');
        this.getFaqCategoryList();
    },

    methods: {
        getFaqCategoryList() {
            this.repository
                .search(new Criteria(), Shopware.Context.api)
                .then((result) => {
                    this.faqCategory = result;
                });
        },

        abortOnLanguageChange() {
            return Shopware.State.getters['swProductDetail/hasChanges'];
        },

        saveOnLanguageChange() {
            return this.onSave();
        },

        onChangeLanguage(languageId) {
            Shopware.State.commit('context/setApiLanguageId', languageId);
            this.getFaqCategoryList();
        },
    }
});
