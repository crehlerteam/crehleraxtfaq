import template from './faq-category-bundle-detail.html.twig';

const { Component, Mixin } = Shopware;

Component.register('faq-category-bundle-detail', {
    template,

    inject: [
        'repositoryFactory'
    ],

    mixins: [
        Mixin.getByName('notification')
    ],

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    data() {
        return {
            faqCategory: null,
            isLoading: false,
            processSuccess: false,
            repository: null,
            isNew: false
        };
    },

    created() {
        this.repository = this.repositoryFactory.create('crehler_faq_category');
        this.getFaqCategory();
        this.createdComponent();
    },

    methods: {
        
        createdComponent() {
            if (!this.$route.params.id) {
                // set language to system language
                if (!Shopware.State.getters['context/isSystemDefaultLanguage']) {
                    Shopware.State.commit('context/resetLanguageToDefault');
                }
            }
        },

        getFaqCategory() {
            this.repository
                .get(this.$route.params.id, Shopware.Context.api)
                .then((entity) => {
                    this.faqCategory = entity;
                });
        },

        onClickSave() {
            this.isLoading = true;

            this.repository
                .save(this.faqCategory, Shopware.Context.api)
                .then(() => {
                    this.getFaqCategory();
                    this.isLoading = false;
                    this.processSuccess = true;
                }).catch((exception) => {
                    this.isLoading = false;
                    this.createNotificationError({
                        title: this.$t('faq-category-bundle.detail.errorTitle'),
                        message: exception
                    });
                });
        },

        saveFinish() {
            this.processSuccess = false;
        },

        saveOnLanguageChange() {
            return this.onSave();
        },

        onChangeLanguage(languageId) {
            Shopware.State.commit('context/setApiLanguageId', languageId);
            this.getFaqCategory();
        },
    }
});
