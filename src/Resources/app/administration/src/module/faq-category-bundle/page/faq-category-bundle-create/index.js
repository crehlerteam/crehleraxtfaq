const { Component } = Shopware;

Component.extend('faq-category-bundle-create', 'faq-category-bundle-detail', {
    data() {
        return {
            isNew: true
        }
    },

    methods: {
        getFaqCategory() {
            this.faqCategory = this.repository.create(Shopware.Context.api);
        },

        onClickSave() {
            this.isLoading = true;

            this.repository
                .save(this.faqCategory, Shopware.Context.api)
                .then(() => {
                    this.isLoading = false;
                    this.$router.push({ name: 'faq.category.bundle.detail', params: { id: this.faqCategory.id } });
                }).catch((exception) => {
                    this.isLoading = false;

                    this.createNotificationError({
                        title: this.$t('faq-category-bundle.detail.errorTitle'),
                        message: exception
                    });
                });
        }
    }
});