<?php declare(strict_types=1);

namespace Crehler\Faq;

use Shopware\Core\Framework\Plugin;
use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Plugin\Context\ActivateContext;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;

class CrehlerFaq extends Plugin
{
	public function uninstall(UninstallContext $context): void
    {
        parent::uninstall($context);
        if ($context->keepUserData()) {
            return;
        }
        $connection = $this->container->get(Connection::class);
        $connection->executeUpdate('DROP TABLE IF EXISTS `crehler_faq_category_translation`');
        $connection->executeUpdate('DROP TABLE IF EXISTS `crehler_faq_translation`');
        $connection->executeUpdate('DROP TABLE IF EXISTS `crehler_faq`');
        $connection->executeUpdate('DROP TABLE IF EXISTS `crehler_faq_category`');
    }
}
